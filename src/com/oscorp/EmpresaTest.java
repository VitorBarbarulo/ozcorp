package com.oscorp;

public class EmpresaTest {
	public static void main(String[] args) {

		// Dados do funcion�rio
		System.out.println("Dados dos funcion�rios:");

		System.out.println();

		// DIRETOR
		System.out.println("Funcion�rio:");

		Funcionario vitor = new Funcionario("Vitor Barbarulo", "123", "123", "123", "vitor@ozcorp.com", "123!",
				Sexo.MASCULINO, Sangue.O2, new Cargo("Diretor", 30_000, 0, new Departamento("Administrativo", "ADM")));

		System.out.println("Nome: " + vitor.getNome());
		System.out.println("RG: " + vitor.getRg());
		System.out.println("CPF: " + vitor.getCpf());
		System.out.println("Matricula: " + vitor.getMatricula());
		System.out.println("E-mail: " + vitor.getEmail());
		System.out.println("Senha: " + vitor.getSenha());
		System.out.println("Cargo: " + vitor.getCargo().getTitulo());
		System.out.println("Sal�rio: " + vitor.getCargo().getSalario());
		System.out.println("Tipo Sanguineo: " + vitor.getSangue().tipo);
		System.out.println("Sexo: " + vitor.getSexo().s);
		System.out.println("N�vel: " + vitor.getCargo().getNivelAcesso());
		System.out.println("DP: " + vitor.getCargo().getDepartamento().getNome());
		System.out.println("Sigla: " + vitor.getCargo().getDepartamento().getSigla());

		System.out.println();

		// SECRET�RIO
		System.out.println("Funcion�rio:");

		Funcionario lucas = new Funcionario("Lucas", "123", "123", "123", "lucas@ozcorp.com", "123!", Sexo.MASCULINO,
				Sangue.A1, new Cargo("Secret�rio", 3000, 1, new Departamento("Recursos Humanos", "RH")));

		System.out.println("Nome: " + lucas.getNome());
		System.out.println("RG: " + lucas.getRg());
		System.out.println("CPF: " + lucas.getCpf());
		System.out.println("Matricula: " + lucas.getMatricula());
		System.out.println("E-mail: " + lucas.getEmail());
		System.out.println("Senha: " + lucas.getSenha());
		System.out.println("Cargo: " + lucas.getCargo().getTitulo());
		System.out.println("Sal�rio: " + lucas.getCargo().getSalario());
		System.out.println("Tipo Sanguineo: " + lucas.getSangue().tipo);
		System.out.println("Sexo: " + lucas.getSexo().s);
		System.out.println("N�vel: " + lucas.getCargo().getNivelAcesso());
		System.out.println("DP: " + lucas.getCargo().getDepartamento().getNome());
		System.out.println("Sigla: " + lucas.getCargo().getDepartamento().getSigla());

		System.out.println();

		// GERENTE
		System.out.println("Funcion�rio:");

		Funcionario giulia = new Funcionario("Giulia", "123", "123", "123", "giulia@ozcorp.com", "123!", Sexo.FEMININO,
				Sangue.AB1, new Cargo("Gerente", 15_000, 2, new Departamento("Financeiro", "DF")));

		System.out.println("Nome: " + giulia.getNome());
		System.out.println("RG: " + giulia.getRg());
		System.out.println("CPF: " + giulia.getCpf());
		System.out.println("Matricula: " + giulia.getMatricula());
		System.out.println("E-mail: " + giulia.getEmail());
		System.out.println("Senha: " + giulia.getSenha());
		System.out.println("Cargo: " + giulia.getCargo().getTitulo());
		System.out.println("Sal�rio: " + giulia.getCargo().getSalario());
		System.out.println("Tipo Sanguineo: " + giulia.getSangue().tipo);
		System.out.println("Sexo: " + giulia.getSexo().s);
		System.out.println("N�vel: " + giulia.getCargo().getNivelAcesso());
		System.out.println("DP: " + giulia.getCargo().getDepartamento().getNome());
		System.out.println("Sigla: " + giulia.getCargo().getDepartamento().getSigla());

		System.out.println();

		// ENGENHEIRO
		System.out.println("Funcion�rio:");

		Funcionario adison = new Funcionario("Adison", "123", "123", "123", "adison@ozcorp.com", "123!", Sexo.MASCULINO,
				Sangue.O1, new Cargo("Engenheiro", 8000, 3, new Departamento("Comercial", "CM")));

		System.out.println("Nome: " + adison.getNome());
		System.out.println("RG: " + adison.getRg());
		System.out.println("CPF: " + adison.getCpf());
		System.out.println("Matricula: " + adison.getMatricula());
		System.out.println("E-mail: " + adison.getEmail());
		System.out.println("Senha: " + adison.getSenha());
		System.out.println("Cargo: " + adison.getCargo().getTitulo());
		System.out.println("Sal�rio: " + adison.getCargo().getSalario());
		System.out.println("Tipo Sanguineo: " + adison.getSangue().tipo);
		System.out.println("Sexo: " + adison.getSexo().s);
		System.out.println("N�vel: " + adison.getCargo().getNivelAcesso());
		System.out.println("DP: " + adison.getCargo().getDepartamento().getNome());
		System.out.println("Sigla: " + adison.getCargo().getDepartamento().getSigla());

		System.out.println();

		// ANALISTA
		System.out.println("Funcion�rio:");
		
		Funcionario fabricio = new Funcionario("Fabr�cio", "123", "123", "123", "fabricio@ozcorp.com", "123!",
				Sexo.MASCULINO, Sangue.B1, new Cargo("Analista", 5000, 4, new Departamento("Operacional", "OP")));

		System.out.println("Nome: " + fabricio.getNome());
		System.out.println("RG: " + fabricio.getRg());
		System.out.println("CPF: " + fabricio.getCpf());
		System.out.println("Matricula: " + fabricio.getMatricula());
		System.out.println("E-mail: " + fabricio.getEmail());
		System.out.println("Senha: " + fabricio.getSenha());
		System.out.println("Cargo: " + fabricio.getCargo().getTitulo());
		System.out.println("Sal�rio: " + fabricio.getCargo().getSalario());
		System.out.println("Tipo Sanguineo: " + fabricio.getSangue().tipo);
		System.out.println("Sexo: " + fabricio.getSexo().s);
		System.out.println("N�vel: " + fabricio.getCargo().getNivelAcesso());
		System.out.println("DP: " + fabricio.getCargo().getDepartamento().getNome());
		System.out.println("Sigla: " + fabricio.getCargo().getDepartamento().getSigla());

	}
}