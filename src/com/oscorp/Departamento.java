package com.oscorp;

public class Departamento {

	// Atributos
	private String nome;
	private String sigla;

	// Construtor
	public Departamento(String nome, String sigla) {
		super();
		this.nome = nome;
		this.sigla = sigla;
	}

	// Getters & Setters
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
}