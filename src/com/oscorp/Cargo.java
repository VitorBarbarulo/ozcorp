package com.oscorp;

public class Cargo {

	private String titulo;
	private double salario;
	private int nivelAcesso;
	Departamento departamento;

	// Construtor
	public Cargo(String titulo, double salario, int nivelAcesso, Departamento departamento) {
		super();
		this.titulo = titulo;
		this.salario = salario;
		this.nivelAcesso = nivelAcesso;
		this.departamento = departamento;
	}

	// Getters & Setters
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public int getNivelAcesso() {
		return nivelAcesso;
	}

	public void setNivelAcesso(int nivelAcesso) {
		this.nivelAcesso = nivelAcesso;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

}