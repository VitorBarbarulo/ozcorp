package com.oscorp;

public enum Sangue {

	A1("A+"), A2("A-"), B1("B+"), B2("B-"), AB1("AB+"), AB2("AB-"), O1("O+"), O2("O-");

	public String tipo;

	Sangue(String tipo) {
		this.tipo = tipo;

	}
}